import React from 'react';
import ReactDOM from 'react-dom';

import './styles/styles.css';
import App from "./js/components/App";


const app = document.getElementsByClassName('application')[0];

ReactDOM.render(<App/>,  app);


