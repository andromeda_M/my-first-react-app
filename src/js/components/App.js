import React from "react";
import Header from "./Header/Header";
 import Filter from "./Filter/Filter"

export default class App extends React.Component {
  render() {
    return (
      <div>
      <Header />
      <Filter />
      </div>
  )
  }
}
