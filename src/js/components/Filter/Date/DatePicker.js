import DatePicker from "reactDatepicker/node_modules/react-datepicker/dist/react-datepicker";
import React from "react";
import moment from "moment";
import "reactDatepicker/node_modules/react-datepicker/dist/react-datepicker.css";
export default class PickerDate extends React.Component {
  constructor() {
    super();
    this.state = {startDate: moment()};
    this.handleChange =this.handleChange.bind(this)
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });

  }
  render() {

    return (
      <div>
        <DatePicker dateFormat="DD MMM YYYY" selected={this.state.startDate} onChange={this.handleChange} locale='en'/>
      </div>
    )
  }
}
