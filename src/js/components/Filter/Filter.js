import React from "react";
import {Col, Row} from "react-bootstrap";
import {ControlLabel} from "react-bootstrap";
import PickerDate from "./Date/DatePicker";
import './Filter.css';

export default class Filter extends React.Component {
  render() {

    return (
      <Row className="filter show-grid">
        <Col  xs={1} xsOffset={2}className="date">Start Date </Col>
        <Col xs={2} className="datePicker"><PickerDate /></Col>
        <Col xs={1} className="date"> End Date</Col>
        <Col xs={1} className="datePicker"><PickerDate /> </Col>
      </Row>
    )
  }
}
