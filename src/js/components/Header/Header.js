import React from "react";
import {Col, Row} from "react-bootstrap";
import "./Header.css";
export default class Header extends React.Component {
  render() {
    return (
      <Row className = "title show-grid" >
        <Col mdOffset={2} mdOffset={2} smOffset={2} xsOffset={2} className="logo">
          <img src={"/src/images/header-title.png"}></img>
          <p className="sublogo">
            BETTING ACTIVITY
          </p>
        </Col>
      </Row>
    )
  }
}
