var webpack = require('webpack');

// var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var path = require('path');
//var debug = process.env.NODE_ENV NODE_ENV!== 'production';
var debug = true;
module.exports = {
  devtool: debug ? 'inline-sourcemap': null,
  devServer: { hot: true },
 // context: path.join(__dirname + "/src"),
  entry:{
    app: ['./src/index.js']

  }
   // styles: './src/main.scss'
  ,
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: "/dist/",
   // filename: "bundle.js"
  //  path: __dirname+"/dist/",
    filename: './bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
    modulesDir: ['node_modules'],
    root: __dirname+ "/vendor"

  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel'

      },

      {
        test: /\.css$/,

        loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader')
      },
      {
        test: /\.(png|jpg|svg)$/, loader:"file?name=[name].[ext]"
      }
    ]
  },


  plugins: [

    new ExtractTextPlugin('bundle.css', {allChunks: true}),
  //
   //new HtmlWebpackPlugin({
    // template: './src/index.html'
    //}),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
};
